import argparse
import json
import os

from generators.payload.random_payload_generator import RandomPayloadGenerator


class EttnGenerator:
    def __init__(self,
                 output_dir: str):
        self.payload_generator = RandomPayloadGenerator()
        self.output_dir = output_dir

    def _save_ids(self, output_dir: str, file_name: str = "ids.json", **kwargs) -> None:
        os.makedirs(output_dir, exist_ok=True)
        output_file = os.path.join(output_dir, file_name)
        if os.path.exists(output_file):
            with open(output_file, "r") as fp:
                data = json.load(fp)
        else:
            data = {}

        for key, value in kwargs.items():
            data[key] = value

        with open(output_file, "w") as fp:
            json.dump(data, fp, indent=2, ensure_ascii=False)

    def generate_01_01_data(self) -> None:
        tc_output_dir = os.path.join(self.output_dir, "01_01")
        ettn_payload = self.payload_generator.generate_01_01_payload(output_dir=tc_output_dir)
        ettn_id = None

        # TODO: finish the implementation
        raise NotImplementedError("Finish me")

        assert ettn_id is not None, "ETTN ID was not set"
        self._save_ids(output_dir=tc_output_dir,
                       ettn_id=ettn_id)

    def generate_01_02_data(self) -> None:
        tc_output_dir = os.path.join(self.output_dir, "01_02")
        ettn_payload, act_payload = self.payload_generator.generate_01_02_payload(output_dir=tc_output_dir)
        ettn_id = act_id = None

        # TODO: finish the implementation
        raise NotImplementedError("Finish me")

        assert ettn_id is not None, "ETTN ID was not set"
        assert act_id is not None, "ACT ID was not set"
        self._save_ids(output_dir=tc_output_dir,
                       ettn_id=ettn_id,
                       act_id=act_id)

    def generate_01_03_data(self) -> None:
        tc_output_dir = os.path.join(self.output_dir, "01_03")
        ettn_payload, act_payload = self.payload_generator.generate_01_03_payload(output_dir=tc_output_dir)
        ettn_id = act_id = None

        # TODO: finish the implementation
        raise NotImplementedError("Finish me")

        assert ettn_id is not None, "ETTN ID was not set"
        assert act_id is not None, "ACT ID was not set"
        self._save_ids(output_dir=tc_output_dir,
                       ettn_id=ettn_id,
                       act_id=act_id)

    def generate_01_04_data(self) -> None:
        tc_output_dir = os.path.join(self.output_dir, "01_04")
        ettn_payload, act_payload = self.payload_generator.generate_01_04_payload(output_dir=tc_output_dir)
        ettn_id = act_id = None

        # TODO: finish the implementation
        raise NotImplementedError("Finish me")

        assert ettn_id is not None, "ETTN ID was not set"
        assert act_id is not None, "ACT ID was not set"
        self._save_ids(output_dir=tc_output_dir,
                       ettn_id=ettn_id,
                       act_id=act_id)

    def generate_01_07_data(self) -> None:
        tc_output_dir = os.path.join(self.output_dir, "01_07")
        ettn_payload, act_payload = self.payload_generator.generate_01_07_payload(output_dir=tc_output_dir)
        ettn_id = act_id = None

        # TODO: finish the implementation
        raise NotImplementedError("Finish me")

        assert ettn_id is not None, "ETTN ID was not set"
        assert act_id is not None, "ACT ID was not set"
        self._save_ids(output_dir=tc_output_dir,
                       ettn_id=ettn_id,
                       act_id=act_id)

    def generate_01_08_data(self) -> None:
        tc_output_dir = os.path.join(self.output_dir, "01_08")
        ettn_payload, act_payload = self.payload_generator.generate_01_08_payload(output_dir=tc_output_dir)
        ettn_id = act_id = None

        # TODO: finish the implementation
        raise NotImplementedError("Finish me")

        assert ettn_id is not None, "ETTN ID was not set"
        assert act_id is not None, "ACT ID was not set"
        self._save_ids(output_dir=tc_output_dir,
                       ettn_id=ettn_id,
                       act_id=act_id)

    def generate_01_09_data(self) -> None:
        tc_output_dir = os.path.join(self.output_dir, "01_09")
        ettn_payload, act_payload = self.payload_generator.generate_01_09_payload(output_dir=tc_output_dir)
        ettn_id = act_id = None

        # TODO: finish the implementation
        raise NotImplementedError("Finish me")

        assert ettn_id is not None, "ETTN ID was not set"
        assert act_id is not None, "ACT ID was not set"
        self._save_ids(output_dir=tc_output_dir,
                       ettn_id=ettn_id,
                       act_id=act_id)


def main(output_dir: str) -> None:
    ettn_generator = EttnGenerator(output_dir=output_dir)

    for func in (ettn_generator.generate_01_01_data,
                 ettn_generator.generate_01_02_data,
                 ettn_generator.generate_01_03_data,
                 ettn_generator.generate_01_04_data,
                 ettn_generator.generate_01_07_data,
                 ettn_generator.generate_01_08_data,
                 ettn_generator.generate_01_09_data):
        func()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="ETTN generator")
    parser.add_argument("--output-dir",
                        required=True,
                        type=str,
                        help="Destination directory that will contain output files")
    args = parser.parse_args()

    main(output_dir=args.output_dir)
