from enum import Enum


class TransactionType(Enum):
    PLANNED = "PLANNED"
    PICKUP = "PICKUP"
    ARRIVAL = "ARRIVAL"

    def __str__(self) -> str:
        return self.value
