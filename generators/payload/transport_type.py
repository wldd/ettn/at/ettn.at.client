from enum import Enum


class TransportType(Enum):
    CAR = "1"
    LORRY = "2"
    BUS = "3"
    TRAILER = "4"
    SEMI_TRAILER = "5"
    MOTORCYCLE = "6"
    SCOOTER = "7"
    TRICYCLE = "8"
    QUADBIKE = "9"
    TROLLEY = "1001"
    TRAM = "1002"
    MOTOR_TRAILER = "1003"

    def __str__(self) -> str:
        return self.value
