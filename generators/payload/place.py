from generators.payload.place_type import PlaceType


class Place:
    def __init__(self,
                 place_type: PlaceType,
                 koatg: str) -> None:
        self.place_type = place_type
        self.koatg = koatg

    def to_json(self) -> dict:
        return {
            "type": str(self.place_type),
            "koatg": self.koatg
        }
