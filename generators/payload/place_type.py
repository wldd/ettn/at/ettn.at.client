from enum import Enum


class PlaceType(Enum):
    LOAD = "LOAD"
    UNLOAD = "UNLOAD"

    def __str__(self) -> str:
        return self.value
