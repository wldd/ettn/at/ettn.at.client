import datetime

from generators.payload.identification_type import IdentificationType


class IdentificationItem:
    def __init__(self,
                 document_type: IdentificationType,
                 series: str,
                 series_no: str,
                 expires: str) -> None:
        self.document_type = document_type
        self.series = series
        self.series_no = series_no
        self.expires = expires

    def to_json(self) -> dict:
        return {
            "type": str(self.document_type),
            "series": self.series,
            "seriesNo": self.series_no,
            "expires": self.expires
        }
