from generators.payload.transport_type import TransportType


class Transport:
    def __init__(self,
                 transport_type: TransportType,
                 tn: str,
                 country: str) -> None:
        self.transport_type = transport_type
        self.tn = tn
        self.country = country

    def to_json(self) -> dict:
        return {
            "type": str(self.transport_type),
            "tn": self.tn,
            "country": self.country
        }
