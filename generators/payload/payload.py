from typing import List

from generators.payload.actor import Actor
from generators.payload.ettn_type import EttnType
from generators.payload.place import Place
from generators.payload.transport import Transport


class Payload:
    def __init__(self,
                 ettn_type: EttnType,
                 actors: List[Actor],
                 transports: List[Transport],
                 places: List[Place],
                 internal_id: str,
                 tonnage: float) -> None:
        self.ettn_type = ettn_type
        self.actors = actors
        self.transports = transports
        self.places = places
        self.internal_id = internal_id
        self.tonnage = tonnage

    def to_json(self) -> dict:
        return {
            "type": str(self.ettn_type),
            "actors": [actor.to_json() for actor in self.actors],
            "transports": [transport.to_json() for transport in self.transports],
            "places": [place.to_json() for place in self.places],
            "internalId": self.internal_id,
            "tonnage": self.tonnage
        }
