import argparse
import datetime
import json
import mimesis
import os
import pytz

from mimesis.enums import Locale

from generators.payload.actor import Actor
from generators.payload.actor_representative import ActorRepresentative
from generators.payload.actor_representative_role import ActorRepresentativeRole
from generators.payload.actor_role import ActorRole
from generators.payload.identification_type import IdentificationType
from generators.payload.identification_document import IdentificationItem
from generators.payload.payload import Payload
from generators.payload.ettn_type import EttnType
from generators.payload.place import Place
from generators.payload.place_type import PlaceType
from generators.payload.transport import Transport
from generators.payload.transport_type import TransportType


ua_alphabet = "АБВГДЕЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЮЯЬ"


class RandomPayloadGenerator:
    def __init__(self) -> None:
        self.generic = mimesis.Generic(Locale.UK)

    def generate_passport(self) -> IdentificationItem:
        timezone = self.generic.random.choice(pytz.all_timezones)
        return IdentificationItem(document_type=IdentificationType.PASSPORT,
                                  series=self.generic.random.generate_string(str_seq=ua_alphabet, length=2),
                                  series_no=self.generic.person.identifier("######"),
                                  expires=self.generic.datetime.formatted_datetime(
                                          "%Y-%m-%dT%H:%M%z",
                                          start=datetime.datetime.today().year,
                                          end=datetime.datetime.today().year + 40,
                                          timezone=timezone))

    def generate_edrpou(self) -> str:
        return self.generic.person.identifier("##########")

    def generate_driver_licence(self) -> IdentificationItem:
        timezone = self.generic.random.choice(pytz.all_timezones)
        return IdentificationItem(document_type=IdentificationType.DRIVER_LICENCE,
                                  series=self.generic.random.generate_string(str_seq=ua_alphabet, length=3),
                                  series_no=self.generic.person.identifier("######"),
                                  expires=self.generic.datetime.formatted_datetime(
                                          "%Y-%m-%dT%H:%M%z",
                                          start=datetime.datetime.today().year,
                                          end=datetime.datetime.today().year + 20,
                                          timezone=timezone
                                      ))

    def generate_representative(self, role: ActorRepresentativeRole) -> ActorRepresentative:
        if role in (ActorRepresentativeRole.driver, ActorRepresentativeRole.senior_driver,
                    ActorRepresentativeRole.forwarder_driver):
            identification_document = self.generate_driver_licence()
        else:
            identification_document = self.generate_passport()

        return ActorRepresentative(name=self.generic.person.full_name(),
                                   role=role,
                                   identification_document=identification_document)

    def generate_actor(self, actor_role: ActorRole) -> Actor:
        if actor_role in (ActorRole.CONSIGNOR, ActorRole.CONSIGNEE, ActorRole.CUSTOMER):
            actor_representative_role = ActorRepresentativeRole.brigadier
        elif actor_role == ActorRole.CARRIER:
            actor_representative_role = ActorRepresentativeRole.driver
        elif actor_role == ActorRole.STORAGE:
            actor_representative_role = ActorRepresentativeRole.forwarder_driver
        else:
            raise NotImplementedError(f"Unsupported actor: '{actor_role.name}'")

        return Actor(edrpou=self.generate_edrpou(),
                     role=actor_role,
                     representative=self.generate_representative(role=actor_representative_role))

    def generate_transport(self, transport_type: TransportType, country: str = "Ukraine") -> Transport:
        return Transport(transport_type=transport_type,
                         tn=f"{self.generic.random.generate_string(str_seq=ua_alphabet, length=2)}"
                            f"{self.generic.person.identifier('####')}"
                            f"{self.generic.random.generate_string(str_seq=ua_alphabet, length=2)}",
                         country=country)

    def generate_place(self, place_type: PlaceType) -> Place:
        return Place(place_type=place_type,
                     koatg=f"UA{self.generic.person.identifier('############')}")

    def generate_tonnage(self) -> float:
        return self.generic.numeric.float_number(start=100, end=30000, precision=2)

    def generate_internal_id(self) -> str:
        return self.generic.cryptographic.uuid()

    @staticmethod
    def _save_payload(payload: dict,
                      output_path: str) -> None:
        output_dir = os.path.dirname(output_path)
        os.makedirs(output_dir, exist_ok=True)

        with open(output_path, "w") as fp:
            json.dump(payload, fp, indent=2, ensure_ascii=False)

    def generate_01_01_payload(self,
                               output_dir: str = None) -> dict:
        payload = Payload(ettn_type=mimesis.random.get_random_item(EttnType),
                          actors=[self.generate_actor(actor_role=ActorRole.CONSIGNOR),
                                  self.generate_actor(actor_role=ActorRole.CARRIER),
                                  self.generate_actor(actor_role=ActorRole.CONSIGNEE)],
                          transports=[self.generate_transport(transport_type=TransportType.CAR)],
                          places=[self.generate_place(place_type=PlaceType.LOAD),
                                  self.generate_place(place_type=PlaceType.UNLOAD)],
                          internal_id=self.generate_internal_id(),
                          tonnage=self.generate_tonnage())

        payload_json = payload.to_json()
        if output_dir:
            RandomPayloadGenerator._save_payload(payload=payload_json,
                                                 output_path=os.path.join(output_dir, "payload.json"))

        return payload_json

    def generate_01_02_payload(self,
                               output_dir: str = None) -> (dict, dict):
        ettn_payload = self.generate_01_01_payload(output_dir=output_dir)
        act_payload = [self.generate_actor(actor_role=actor_role) for actor_role in (ActorRole.CARRIER,
                                                                                     ActorRole.CUSTOMER,
                                                                                     ActorRole.CONSIGNOR)]
        actors_json = {"actors": [actor.to_json() for actor in act_payload]}
        if output_dir:
            RandomPayloadGenerator._save_payload(payload=actors_json,
                                                 output_path=os.path.join(output_dir, "actors.json"))

        return ettn_payload, actors_json

    def generate_01_03_payload(self,
                               output_dir: str = None) -> (dict, dict):
        ettn_payload = self.generate_01_01_payload(output_dir=output_dir)
        act_payload = [self.generate_actor(actor_role=actor_role) for actor_role in (ActorRole.CARRIER,
                                                                                     ActorRole.STORAGE)]

        actors_json = {"actors": [actor.to_json() for actor in act_payload]}
        if output_dir:
            RandomPayloadGenerator._save_payload(payload=actors_json,
                                                 output_path=os.path.join(output_dir, "actors.json"))

        return ettn_payload, actors_json

    def generate_01_04_payload(self,
                               output_dir: str = None) -> (dict, dict):
        return self.generate_01_03_payload(output_dir=output_dir)

    def generate_01_07_payload(self,
                               output_dir: str = None) -> (dict, dict):
        ettn_payload = self.generate_01_01_payload(output_dir=output_dir)
        act_payload = [self.generate_actor(actor_role=actor_role) for actor_role in (ActorRole.CONSIGNOR,
                                                                                     ActorRole.CONSIGNEE,
                                                                                     ActorRole.CARRIER)]
        actors_json = {"actors": [actor.to_json() for actor in act_payload]}
        if output_dir:
            RandomPayloadGenerator._save_payload(payload=actors_json,
                                                 output_path=os.path.join(output_dir, "actors.json"))

        return ettn_payload, actors_json

    def generate_01_08_payload(self,
                               output_dir: str = None) -> (dict, dict):
        ettn_payload = self.generate_01_01_payload(output_dir=output_dir)
        act_payload = [self.generate_actor(actor_role=actor_role) for actor_role in (ActorRole.CONSIGNOR,
                                                                                     ActorRole.CARRIER)]

        actors_json = {"actors": [actor.to_json() for actor in act_payload]}
        if output_dir:
            RandomPayloadGenerator._save_payload(payload=actors_json,
                                                 output_path=os.path.join(output_dir, "actors.json"))

        return ettn_payload, actors_json

    def generate_01_09_payload(self,
                               output_dir: str = None) -> (dict, dict):
        ettn_payload = self.generate_01_01_payload(output_dir=output_dir)
        act_payload = [self.generate_actor(actor_role=actor_role) for actor_role in (ActorRole.CUSTOMER,
                                                                                     ActorRole.CARRIER)]

        actors_json = {"actors": [actor.to_json() for actor in act_payload]}
        if output_dir:
            RandomPayloadGenerator._save_payload(payload=actors_json,
                                                 output_path=os.path.join(output_dir, "actors.json"))

        return ettn_payload, actors_json


def main(output_dir: str) -> None:
    generator = RandomPayloadGenerator()
    generator.generate_01_01_payload(output_dir=os.path.join(output_dir, "01_01"))
    generator.generate_01_02_payload(output_dir=os.path.join(output_dir, "01_02"))
    generator.generate_01_03_payload(output_dir=os.path.join(output_dir, "01_03"))
    generator.generate_01_04_payload(output_dir=os.path.join(output_dir, "01_04"))
    generator.generate_01_07_payload(output_dir=os.path.join(output_dir, "01_07"))
    generator.generate_01_08_payload(output_dir=os.path.join(output_dir, "01_08"))
    generator.generate_01_09_payload(output_dir=os.path.join(output_dir, "01_09"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="ETTN Payload generator")
    parser.add_argument("--output-dir",
                        required=False,
                        default=None,
                        help="Destination directory for the files that will contain generated payload")
    args = parser.parse_args()
    main(output_dir=args.output_dir)
