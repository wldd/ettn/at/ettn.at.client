from generators.payload.actor_representative_role import ActorRepresentativeRole
from generators.payload.identification_document import IdentificationItem


class ActorRepresentative:
    def __init__(self,
                 name: str,
                 role: ActorRepresentativeRole,
                 identification_document: IdentificationItem,
                 signature_required: bool = True) -> None:
        self.name = name
        self.role = role
        self.identification_document = identification_document
        self.signature_required = signature_required

    def to_json(self) -> dict:
        return {
            "name": self.name,
            "role": str(self.role),
            "identification": [
                self.identification_document.to_json()
            ],
            "signatureRequired": self.signature_required
        }
