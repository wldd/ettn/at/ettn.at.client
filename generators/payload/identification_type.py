from enum import Enum


class IdentificationType(Enum):
    PASSPORT = "PASSPORT"
    DRIVER_LICENCE = "DRIVER_LICENCE"
    RNOKPP = "RNOKPP"
    EDRPOU = "EDRPOU"

    def __str__(self) -> str:
        return self.value
