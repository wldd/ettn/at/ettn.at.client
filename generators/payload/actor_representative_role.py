from enum import Enum


class ActorRepresentativeRole(Enum):
    brigadier = "1"
    accountant = "2"
    weigher = "3"
    driver = "4"
    forwarder_driver = "5"
    senior_driver = "6"
    director = "7"
    dispatcher = "8"
    forwarder = "9"
    barn_manager = "10"
    store_manager = "11"
    warehouse_manager = "12"
    chief = "13"
    storekeeper = "14"
    picker = "15"
    controller = "16"
    master = "17"
    manager = "18"
    operator = "19"
    acceptor = "20"
    commodity_expert = "21"
    lawyer = "22"

    def __str__(self) -> str:
        return self.value
