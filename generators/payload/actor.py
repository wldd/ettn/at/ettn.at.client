from generators.payload.actor_representative import ActorRepresentative
from generators.payload.actor_role import ActorRole


class Actor:
    def __init__(self,
                 edrpou: str,
                 role: ActorRole,
                 representative: ActorRepresentative) -> None:
        self.edrpou = edrpou
        self.role = role
        self.representative = representative

    def to_json(self) -> dict:
        return {
            "edrpou": self.edrpou,
            "role": str(self.role),
            "representative": [
                self.representative.to_json()
            ]
        }
