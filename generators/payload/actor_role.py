from enum import Enum


class ActorRole(Enum):
    CONSIGNOR = "CONSIGNOR"
    CARRIER = "CARRIER"
    CONSIGNEE = "CONSIGNEE"
    CUSTOMER = "CUSTOMER"
    EXPEDITOR = "EXPEDITOR"
    STORAGE = "STORAGE"
    THIRD_PARTY = "THIRD_PARTY"

    def __str__(self) -> str:
        return self.value

