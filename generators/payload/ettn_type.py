from enum import Enum


class EttnType(Enum):
    GENERIC = "GENERIC"
    ALCOHOL = "ALCOHOL"
    SPIRIT = "SPIRIT"
    VKD = "VKD"
    OIL = "OIL"
    BREAD = "BREAD"
    MILK = "MILK"
    WOOD = "WOOD"
    MEDICINES = "MEDICINES"
    OTHER = "OTHER"

    def __str__(self) -> str:
        return self.value
